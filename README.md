# entware-base
![Docker Pulls](https://img.shields.io/docker/pulls/forumi0721/entware-base)
![Docker Stars](https://img.shields.io/docker/stars/forumi0721/entware-base)



----------------------------------------
### x64
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721/entware-base/x64)
### aarch64
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721/entware-base/aarch64)
### armv7
![Docker Image Size (tag)](https://img.shields.io/docker/image-size/forumi0721/entware-base/armv7)



----------------------------------------
#### Description

* Distribution : [Entware](https://github.com/Entware/Entware/)
* Architecture : x64,aarch64,armv7
* Appplication : -



----------------------------------------
#### Run

```sh
docker run -i -t --rm \
           forumi0721/entware-base:[ARCH_TAG]
```



----------------------------------------
#### Usage

```dockerfile
FROM forumi0721/entware-base:[ARCH_TAG]

RUN 'build-code'
```



----------------------------------------
#### Docker Options

| Option             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Ports

| Port               | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Volumes

| Volume             | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |


#### Environment Variables

| ENV                | Description                                      |
|--------------------|--------------------------------------------------|
| -                  | -                                                |

